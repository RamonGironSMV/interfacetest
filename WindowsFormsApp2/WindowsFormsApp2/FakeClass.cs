﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterfaceImageOperators;
using OpenCvSharp;

namespace WindowsFormsApp2
{
    public class FakeClass : IImageOperators
    {
        public Mat AddConstant(Mat img, float constantValue)
        {
            throw new NotImplementedException();
        }

        public Mat BasicConvolutionImage(Mat img, Mat convolutionMask)
        {
            throw new NotImplementedException();
        }

        public Mat BinaryBoundary(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat ClearImage(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat ClearImage(Mat img, float value)
        {
            Mat clearimg = img.Clone();
            for (int i = 0; i < clearimg.Height; i++)
            {
                for (int j = 0; j < clearimg.Width; j++)
                {
                    var pixel = img.Get<Vec3b>(i, j);
                    var i0 = value;
                    var i1 = value;
                    var i2 = value;
                    var newPixel = new Vec3b { Item0 = (byte)(i2), Item1 = (byte)(i1), Item2 = (byte)(i0) };
                    clearimg.Set(i, j, newPixel);
                }
            }
            return clearimg;
        }

        public Mat CopyImage(Mat img)
        {
            Size s = img.Size();

            Mat copyimg = new Mat(s.Height, s.Width, MatType.CV_8UC3);
            for (int i = 0; i < img.Height; i++)
            {
                for (int j = 0; j < img.Width; j++)
                {
                    var pixel = img.Get<Vec3b>(i, j);
                    var newPixel = new Vec3b { Item0 = pixel.Item0, Item1 = pixel.Item1, Item2 = pixel.Item2 };
                    copyimg.Set(i, j, newPixel);
                }
            }
            return copyimg;
        }

        public Mat Dilation(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat Erosion(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat InvertImage(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat RemoveNoise(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat RemoveNoiseSpurs(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat RemovePepperNoise(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat RemoveSaltNoise(Mat img)
        {
            throw new NotImplementedException();
        }

        public Mat Scaling(Mat img, float gamma, float beta)
        {
            throw new NotImplementedException();
        }

        public Mat ShiftImage(Mat img, DirectionsEnum direction, float steps, float optionalSteps = 0)
        {
            throw new NotImplementedException();
        }

        public Mat ShiftImage(Mat img, float xSteps, float ySteps)
        {
            throw new NotImplementedException();
        }

        public Mat ShiftImage(Mat img, Mat translationMatrix)
        {
            throw new NotImplementedException();
        }

        public Mat ThresholdImage(Mat img, float threshold)
        {
            throw new NotImplementedException();
        }

        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold)
        {
            throw new NotImplementedException();
        }

        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh)
        {
            throw new NotImplementedException();
        }

        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh, float keepVal)
        {
            throw new NotImplementedException();
        }

        public Mat VisualiseBinary(Mat img)
        {
            throw new NotImplementedException();
        }
    }
}
